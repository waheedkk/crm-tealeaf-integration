// Import All required dependencies
var request = require('request');
var xml2js = require('xml2js');
var parseString = require('xml2js').parseString;
var async = require("async");
var moment = require('moment');
var momentTz = require('moment-timezone');
var xmlBuilder = require('xmlbuilder');

var exports = module.exports;

const REVEAL_API_URL = 'http://192.168.16.72/PortalAPI/cxReveal.asmx/';

// Export All methods so that other Classes can access them
exports.authenticate = authenticate;
exports.verifyAuthentication = verifyAuthentication;
exports.getSearchSortFields = getSearchSortFields;
exports.getLastFiveSessions = getLastFiveSessions;
exports.getLastSpecificDaysSessions = getLastSpecificDaysSessions;
exports.getActiveSessions = getActiveSessions;
exports.getContactUsFormSession = getContactUsFormSession;
exports.errorResponse = errorResponse;

/**
 * Method to Hit any Third Party POST API
 *
 * @params 
 * 1- restClientObj =>  An Object contating API URL And/Or form Data
 * 2- cb => Callback Function that contains err or result
 * @return Callback Function
 * 
 * @Developer Waheed Ahmed
 */
function restRequest (restClientObj, cb) {
  request.post(restClientObj, function(err, httpResponse, body){
    if(!err) {
      parseString(body, function (err1, result) {
        if(!err1) {
          cb(null, result);
        } else {
          cb(err1, null);
        }
      });
    } else {
      cb(err, null);
    }
  });
}

/**
 * Method to Authenticate Reveal API to access Tealeaf System
 *
 * @params 
 * 1- credentials =>  An Object contating Username and Password
 * 2- callback => Callback Function that contains err or result
 * @return Callback Function
 * 
 * @Developer Waheed Ahmed
 */
function authenticate (credentials, callback) {
 console.log("Inside authenticate");
  var restClientObj = {
    url: REVEAL_API_URL + 'Authenticate', 
    form: credentials
  }
  restRequest(restClientObj, function(err, result){
    if(!err) {
      if(result.string && result.string._) {
        err = errorResponse(result.string._);
        callback(err, null);
      } else{
        callback(null, result);
      }
    } else {
      console.log("authenticate: ", err);
      err = errorResponse();
      callback(err, null);
    }
  });
}

/**
 * Method to verify Authentication with Tealeaf System
 *
 * @params 
 * 1- prevResp => returned value of Previos Method In this case [authenticate] Method
 * 2- callback => Callback Function that contains err or result
 * @return True/False in Callback Function
 * 
 * @Developer Waheed Ahmed
 */
function verifyAuthentication (prevResp, callback) {
  console.log("Inside VerifyAuthentication");
  var restClientObj = {
    url: REVEAL_API_URL + 'VerifyAuthentication'
  }
  restRequest(restClientObj, function(err, result){
    if(!err) {
      callback(null, result);
    } else {
      console.log("verifyAuthentication: ", err);
      err = errorResponse();
      callback(err, null);
    }
  });
}

/**
 * Method to get Search Sort Fields.
 * So that We can get Sorted sessions List on the basis of the Parameters,
 * returning by this Method 
 *
 * @params 
 * 1- prevResp => returned value of Previos Method In this case [verifyAuthentication] Method
 * 2- callback => Callback Function that contains err or result
 * @return Array of Possible Options for Sorting in Callback Function
 * 
 * @Developer Waheed Ahmed
 */
function getSearchSortFields (prevResp, callback) {

  console.log("Inside getSearchSortFields");  
  var restClientObj = {
    url: REVEAL_API_URL + 'GetSearchSortFields'
  }
  restRequest(restClientObj, function(err, result){
    if(!err) {
      callback(null, result);
    } else {
      console.log("GetSearchSortFields: ", err);
      err = errorResponse();
      callback(err, null);
    }
  });
}

/**
 * Method to get Search Results If Search Done Successfully.
 *
 * @params 
 * 1- searchId => Search ID returns by start Search Method
 * 2- emailId => email of the Customer
 * 3- callback => Callback Function that contains err or result
 * @return Array of Sessions in Callback Function
 * 
 * @Developer Waheed Ahmed
 */
function getSearchResults (searchId, sessionLimit, searchType, emailId, callback) {

  console.log("Inside getSearchResults");
  var restClientObj = {
    url: REVEAL_API_URL + 'GetSearchResults', 
    form: {
      SearchID: searchId,
      MaxResults: sessionLimit
    }
  };
  restRequest(restClientObj, function(err, result){
    if(!err) {
      if(result.ArrayOfTLMiniSession.TLMiniSession && result.ArrayOfTLMiniSession.TLMiniSession.length > 0){
        var sessionsArray = [];    
        sessionLimit = result.ArrayOfTLMiniSession.TLMiniSession.length > sessionLimit ? sessionLimit : result.ArrayOfTLMiniSession.TLMiniSession.length;      
        for(var i=0; i < sessionLimit; i++) {
          var finalRespBody = result.ArrayOfTLMiniSession.TLMiniSession[i];
          console.log("finalRespBody: ", finalRespBody);
          if(searchType == 'active') {
            if (finalRespBody.IsActiveSession[0] == 'true') {
              // Push Each session into Sessions Array
              sessionsArray.push(prepareResponse(finalRespBody, emailId));
            } else {
              err = errorResponse('No Active session Found');
              callback(err, null);
              return;
            }
          } else {
            // Push Each session into Sessions Array
            sessionsArray.push(prepareResponse(finalRespBody, emailId));
          }
        }
        /*var sessions = {
          SessionsList: {
            session: sessionsArray
          }
        };*/
        var sessions = sessionsArray;
        // Convert JSON to XML
        //builder = new xml2js.Builder();
        //var xmlResp = builder.buildObject(sessions);
        console.log("sessions: ", sessions);
        callback(null, sessions);
      } else {
        console.log("I'm Here");
        err = errorResponse('No session Found');
        callback(err, null);
      }
    }  else {
      console.log("GetSearchResults: ", err);
      err = errorResponse();
      callback(err, null);
    }
  });
}

/**
 * Method to Start Search for Sessions From Tealeaf System And returns them If found any or return error.
 *
 * @params 
 * 1- emailId => email of the Customer
 * 2- onlyLetterEmailId => String containg only AlphaNumeric Characters
 * 3- prevResp => returned value of Previos Method In this case [getSearchSortFields] Method
 * 3- callback => Callback Function that contains err or result
 * @return Array of Sessions in Callback Function
 * 
 * @Developer Waheed Ahmed
 */
function startSearch(searchObj, sessionLimit, searchType, emailId, callback) {

  console.log("Inside startSearch");
  var restClientObj = {
    url: REVEAL_API_URL + 'StartSearch',
    form : searchObj
  };
  restRequest(restClientObj, function(err, result){
    if(!err) {
      var searchId = result.int._;
      var count = 0;
      var setIntervalId = 0;
      async.whilst(
        function() { return count < 12; },
        function(cb) {
          setIntervalId =  setInterval(function(){
            var restClientObj = {
              url: REVEAL_API_URL + 'GetSearchStatus', 
              form: {
                SearchID: searchId
              }
            };
            restRequest(restClientObj, function(err, result){
              console.log("result: ", result);
              if(!err) {
                if(result.TLSearchStatus.Done[0] == 'true' && result.TLSearchStatus.DownloadComplete[0] == 'true' && count<12) {
                  console.log("1");
                  count = 12;
                  cb(null, result);
                } else if ((result.TLSearchStatus.Done[0] == 'false' || result.TLSearchStatus.DownloadComplete[0] == 'false') && count<12) {
                  console.log("2");
                  count++;
                } else if ((result.TLSearchStatus.Done[0] == 'false' || result.TLSearchStatus.DownloadComplete[0] == 'false') && count>=12) {
                  console.log("3");
                  cb('Unfortunately! Search is not performed Successfully. Please Try Again', null);
                }
              } else {
                console.log("GetSearchStatus: ", err);
                err = errorResponse();
                callback(err, null);
              }
            });
          }, 500);
        },
        function (err, searchResp) {
          clearInterval(setIntervalId);
          if(!err) {
            if(searchResp != null) {
              console.log("Hello");
              getSearchResults(searchId, sessionLimit, searchType, emailId, function(err, result){
                if(!err && result)  {
                  callback(null, result);
                } else {
                  callback(err, null);
                }
              }); 
            } 
          } else {
            err = errorResponse(err);
            callback(err, null);
          }
        }
      );
    }  else {
      console.log("StartSearch: ", err);
      err = errorResponse();
      callback(err, null);
    }
  });
}

/**
 * Method to get Last Five Sessions From Tealeaf System And returns them If found any or return error.
 *
 * @params 
 * 1- emailId => email of the Customer
 * 2- onlyLetterEmailId => String containg only AlphaNumeric Characters
 * 3- prevResp => returned value of Previos Method In this case [getSearchSortFields] Method
 * 3- callback => Callback Function that contains err or result
 * @return Array of Sessions in Callback Function
 * 
 * @Developer Waheed Ahmed
 */
function getLastFiveSessions(emailId, onlyLetterEmailId, prevResp, callback) {

  console.log("Inside getLastFiveSessions");
  var CurrentDate = moment().toISOString();
  var startDate = moment().subtract(28, 'days').toISOString();
  var searchObj = {
    ActiveQuery: '(customvar0 contains ' +  emailId + ')',
    ArchiveQuery: '(tltstscustomvar0 contains "' + onlyLetterEmailId + '")',
    StartDate: startDate,
    EndDate: CurrentDate,
    SortField: prevResp.ArrayOfTLSortField.TLSortField[2].SortID[0]
  }
  var sessionLimit = 5;
  startSearch(searchObj, sessionLimit, null, emailId, function (err, result) {
    if(!err && result) {
      callback(null, result);
    } else {
      console.log("getLastFiveSessions: ", err);
      callback(err, null);
    }
  });
}

/**
 * Method to get Last Seven Days Sessions From Tealeaf System And returns them If found any or return error.
 *
 * @params 
 * 1- emailId => email of the Customer
 * 2- onlyLetterEmailId => String containg only AlphaNumeric Characters
 * 3- prevResp => returned value of Previos Method In this case [getSearchSortFields] Method
 * 3- callback => Callback Function that contains err or result
 * @return Array of Sessions in Callback Function
 * 
 * @Developer Waheed Ahmed
 */
function getLastSpecificDaysSessions(emailId, onlyLetterEmailId, searchDuration, prevResp, callback) {

  console.log("Inside getLastSpecificDaysSessions");
  var CurrentDate = moment().toISOString();
  var startDate = moment().subtract(searchDuration, 'days').toISOString();
  var searchObj = {
    ActiveQuery: '(customvar0 contains ' +  emailId + ')',
    ArchiveQuery: '(tltstscustomvar0 contains "' + onlyLetterEmailId + '")',
    StartDate: startDate,
    EndDate: CurrentDate,
    SortField: prevResp.ArrayOfTLSortField.TLSortField[2].SortID[0]
  }
  var sessionLimit = 100;
  startSearch(searchObj, sessionLimit, null, emailId, function (err, result) {
    if(!err && result) {
      callback(null, result);
    } else {
      callback(err, null);
    }
  });
}

/**
 * Method to get Only Live Active Sessions From Tealeaf System And returns them If found any or return error.
 *
 * @params 
 * 1- emailId => email of the Customer
 * 2- onlyLetterEmailId => String containg only AlphaNumeric Characters
 * 3- prevResp => returned value of Previos Method In this case [getSearchSortFields] Method
 * 3- callback => Callback Function that contains err or result
 * @return Array of Sessions in Callback Function
 * 
 * @Developer Waheed Ahmed
 */
function getActiveSessions(emailId, onlyLetterEmailId, searchType, prevResp, callback) {
  
  console.log("Inside getActiveSessions");
  var startTimeStamp = moment().subtract(20, 'hours').toISOString();
  var endTimeStamp = moment().add(4, 'hours').toISOString();
  var searchObj = {
    ActiveQuery: '(customvar0 contains ' +  emailId + ')',
    ArchiveQuery: '(tltstscustomvar0 contains "' + onlyLetterEmailId + '")',
    StartDate: startTimeStamp,
    EndDate: endTimeStamp,
    SortField: prevResp.ArrayOfTLSortField.TLSortField[2].SortID[0]
  }
  var sessionLimit = 1;
  startSearch(searchObj, sessionLimit, searchType, emailId, function (err, result) {
    if(!err && result) {
      callback(null, result);
    } else {
      callback(err, null);
    }
  });
}

/**
 * Method to get Last Seven Days Sessions From Tealeaf System And returns them If found any or return error.
 *
 * @params 
 * 1- emailId => email of the Customer
 * 2- onlyLetterEmailId => String containg only AlphaNumeric Characters
 * 3- prevResp => returned value of Previos Method In this case [getSearchSortFields] Method
 * 3- callback => Callback Function that contains err or result
 * @return Array of Sessions in Callback Function
 * 
 * @Developer Waheed Ahmed
 */
function getContactUsFormSession(emailId, onlyLetterEmailId, pageUrl, onlyLetterPageUrl, prevResp, callback) {

  console.log("Inside getContactUsFormSession");
  var startTimeStamp = moment().subtract(20, 'hours').toISOString();
  var endTimeStamp = moment().add(4, 'hours').toISOString();
  var searchObj = {
    ActiveQuery: '(request/env/url contains ' + pageUrl + ') AND (request/urlfield/Email contains ' + emailId + ')',
    ArchiveQuery: '(url contains "' + onlyLetterPageUrl + '") AND (request/urlfield/Email contains ' + onlyLetterEmailId + ')',
    StartDate: startTimeStamp,
    EndDate: endTimeStamp,
    SortField: prevResp.ArrayOfTLSortField.TLSortField[2].SortID[0]
  }
  console.log("searchObj: ", searchObj);
  var sessionLimit = 1;
  startSearch(searchObj, sessionLimit, null, emailId, function (err, result) {
    if(!err && result) {
      callback(null, result);
    } else {
      callback(err, null);
    }
  });
}

/**
 * Method to prepare Final Reponse for sending It to Thrid Party CRM.
 *
 * @params 
 * 1- resp => Response Object got from Tealeaf Search API 
 * @return Array of Sessions in Callback Function
 * 
 * @Developer Waheed Ahmed
 */
function prepareResponse(resp, emailId) {

  // Format Time in ISO Format + Calculate Duration
  var startTime = new Date(parseInt(resp.FirstUse[0]) * 1000);
  var isoStartTime = momentTz.tz(startTime, "Asia/Karachi").format();
  var endTime = new Date(parseInt(resp.LastUse[0]) * 1000);
  var isoEndTime = momentTz.tz(endTime, "Asia/Karachi").format();
  var duration = moment.duration(moment(endTime).diff(moment(startTime)));                             
  var duration_hours = duration.hours();
  duration_hours = duration_hours < 10 ? '0'+ duration_hours : duration_hours;
  var duration_mins = duration.minutes();
  duration_mins = duration_mins < 10 ? '0'+ duration_mins : duration_mins;
  var duration_seconds = duration.seconds();
  duration_seconds = duration_seconds < 10 ? '0'+ duration_seconds : duration_seconds;
  duration = duration_hours + ':' + duration_mins + ':' + duration_seconds;
  var serverName = resp.ReplayLinkBBR[0].substring(resp.ReplayLinkBBR[0].lastIndexOf("http://")+7, resp.ReplayLinkBBR[0].indexOf("/Portal/"));
  var replayLink = resp.ReplayLinkBBR[0].replace(serverName, "192.168.16.72");
  console.log("replayLink: ", replayLink);
  // Prepare session Objects for response
  var session = {
    SessionId: resp.SessionId[0],
    TltSid: resp.TltSid[0],
    IpAddress: resp.IpAddress[0],
    email: emailId,
    Browser: resp.Browser[0],
    StartTime: isoStartTime,
    EndTime: isoEndTime,
    duration: duration,
    ReplayLinkBBR: replayLink
  };

  return session
}

/**
 * Method to prepare Error Reponse for sending It to Thrid Party CRM.
 *
 * @params 
 * 1- message => Error Message that is sent to Third party CRM 
 * @return XML error containing status code and error Message
 * 
 * @Developer Waheed Ahmed
 */
function errorResponse(message) {
  
  var errorMsg = 'Something went wrong on Server Side';
  if(message) {
    errorMsg = message;
  }
  var response = {
    response: {
      status: 500,
      errorMessage: errorMsg
    }
  };
  // return response Object
  return response;
}